<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2017/4/26
 * Time: 23:58
 */
//把传过来的数据库入库
header("Content-type: text/html; Charset=utf-8");

require_once('../connect.php');
//var_dump($_POST);

$title = preg_replace('/ /','', $_POST['title']); //去除title中的所有空格
if(empty($title)){ //判断标题是否为空
    echo "<script>alert('标题不能为空');window.location.href='article.add.php';</script>";
}

$table = $table_prefix.'article';

//获取unix时间戳时间
$dateline =  time();

//获取键
$keys = join(', ', array_keys($_POST));
$keys = $keys . ', dateline';
//var_dump($keys);

// 获取值
//$values = "'" . join("', '", array_values($_POST)) . "'";
//不用array_values()也可以,join可以接受索引数组，也可以接受关联数组。
$values = "'" . join("', '", $_POST) . "'";
$values = $values . ", $dateline";

$insert_sql = "INSERT into $table($keys) VALUES($values)";
//var_dump($insert_sql);


//缺少了相关代码，插入操作依然可以执行。可能原因：数据库缓冲。
if(myQuery($insert_sql)){
    echo "<script>alert('发布文章成功');window.location.href='article.manage.php';</script>";
} else{
    //echo "<script>alert('发布失败');window.location.href='article.manage.php';</script>";
    echo mysql_error();
}
