<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2017/11/2
 * Time: 23:46
 */
/**
 * 工具库函数文件
 */

/**
 *  错误调试方法
 * @param string $sql sql语句
 * @return resource | true
 *
 */
function myQuery($sql)
{
    $result = mysql_query($sql);
    if(!$result)
    {
        echo 'sql语句执行失败！<br />';
        echo '错误编号：'.mysql_errno().'<br />';
        echo '错误信息：'.mysql_error().'<br />';
        die;
    }
    return $result;
}

   /**
    *  返回多行多列的查询结果
    * @param string $sql sql语句
    * @return mixed array | false
    *若无结果集资源，则返回空数组。
    */
function fetchAll($sql)
{
    //执行sql语句
    if ($result = myQuery($sql)) {
        //执行成功
        //遍历结果集资源
        $rows = array();
        while($row = mysql_fetch_assoc($result))
        {
            $rows[] = $row;
        }
        //释放结果集资源
        mysql_free_result($result);
        return $rows;
    }
    else {
        return false; //在这里，由于在myQuery中查询出现错误就die掉，所以false无法返回
    }
}

   /**
     * 返回一行多列的查询结果
     * @param string $sql 一条sql语句
     * @return mixed array | false
     *若无结果集资源，则返回false
     */
function fetchRow($sql)
{
    //执行sql语句
    if ($result = myQuery($sql)) {
        //执行成功
        $row = mysql_fetch_assoc($result); //没有更多行mysql_fetch_assoc返回false
        //释放结果集资源
        mysql_free_result($result);
        return $row;
    }
    else {
        return false;
    }
}
