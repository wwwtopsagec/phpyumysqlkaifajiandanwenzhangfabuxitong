/*
Navicat MySQL Data Transfer

Source Server         : 本地phpstudy
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : article

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-10-29 17:37:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `wd_article`
-- ----------------------------
DROP TABLE IF EXISTS `wd_article`;
CREATE TABLE `wd_article` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章编号',
  `title` char(100) NOT NULL COMMENT '文章标题',
  `author` char(50) NOT NULL COMMENT '文章作者',
  `description` varchar(255) NOT NULL COMMENT '文章简介',
  `content` text NOT NULL,
  `dateline` int(11) NOT NULL DEFAULT '0' COMMENT '文章发布时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wd_article
-- ----------------------------
